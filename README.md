# LHC tau-pair production constraints on $`a_\tau`$ and $`d_\tau`$
This distribution contains the `FeynRules` and `UFO` model files required to simulate the correlated effects of new physics (NP) contributions that induce $`\tau`$ anomalous magnetic moments $`a_\tau`$ and electric dipole moments $`d_\tau`$ in the tail of the $`pp \to \tau \tau `$ spectrum using `MadGraph_aMC@NLO` at NLO+PS accuracy.

## Reference
Please cite [ArXiv:2307.14133](https://arxiv.org/abs/2307.14133) when using any of the code provided in this distribution.  
For more information, support or comments (always welcome 😊), please contact one of the authors:

**Ulrich Haisch** ([haisch@mpp.mpg.de](mailto:haisch@mpp.mpg.de)),  
**Luc Schnell** ([schnell@mpp.mpg.de](mailto:schnell@mpp.mpg.de)),  
**Joachim Weiss** ([jweiss@mpp.mpg.de](mailto:jweiss@mpp.mpg.de)).


## Change log
| Publication date | Version | Description |
| ----------- | ----------- | ----------- |
| Jul 24, 23 | 1.0 | First version of our code, as published in [ArXiv:2307.14133](https://arxiv.org/abs/2307.14133). |

## Getting started
### Model files
The `FeynRules` model file `TauAMMEDM.fr` contains the relevant NP parameters and Lagrangians. It should be paired with the default `SM.fr` model file provided by `FeynRules` in order to obtain the full Lagrangian with both signal and background contributions.

The file `TauAMMEDM.nlo` contains the relevant counterterms needed for NLO+PS simulations in `MadGraph_aMC@NLO`. We used the package [NLOCT](https://arxiv.org/pdf/1406.3030.pdf) to derive them.

The folder `TauAMMEDM_NLO` contains a ready-to-use `UFO` implementation based on `TauAMMEDM.fr` and `TauAMMEDM.nlo`.


## Import in MadGraph
In order to use the `UFO` files in `MadGraph_aMC@NLO`, copy them to `MadGraphvX.X.XX/models/TauAMMEDM_NLO` in your models folder and then run

> ./mg5_aMC

in the `MadGraphvX.X.XX/bin` directory. In the `MadGraph` interface, type

> import TauAMMEDM_NLO  
> generate p p > ta+ ta- [QCD] QED=1 AMMEDM=1  
> output TauAMMEDM_NLO

to obtain a NLO+PS accurate simulation including the NP effects only. **Note:** we gave the parameters `atau` and `dtau` the interaction orders `{{AMMEDM,1},{QED,-1}}` to make sure that the vertex $`\gamma \tau^+ \tau^-`$ induced by the NP operators has interaction order `{QED,0}`. That is why we can require `AMMEDM=1` at the amplitude level above. The parameters of the simulation may then set in `run_card.dat` and `param_card.dat` of the directory `MadGraphvX.X.XX/bin/TauAMMEDM_NLO/Cards/`. The most relevant parameters of the simulation are listed below.  

## Parameter values
In the input file `powheg.input`, the user may change different parameters of our third-generation gauge $`U`$ implementation. They are summarized in the table below.

| Parameter name | Description |
| ----------- | ----------- |
| `atau` |  **(Real)** $`a_\tau`$ value induced by the heavy NP contribution. See eq. (7) in our paper for more information on the definition. |
| `dtau` | **(Real)** $`d_\tau`$ value induced by the heavy NP contribution. See eq. (7) in our paper for more information on the definition.  |
| `mta` | **(Real)** Mass of the $`\tau`$ lepton. |
