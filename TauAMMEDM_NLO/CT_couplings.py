# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 13.3.0 for Linux x86 (64-bit) (June 3, 2023)
# Date: Mon 17 Jul 2023 19:23:27


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_100_1 = Coupling(name = 'R2GC_100_1',
                      value = '-0.006944444444444444*(ee*complex(0,1)*G**3)/cmath.pi**2',
                      order = {'QCD':3,'QED':1})

R2GC_100_2 = Coupling(name = 'R2GC_100_2',
                      value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_101_3 = Coupling(name = 'R2GC_101_3',
                      value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_101_4 = Coupling(name = 'R2GC_101_4',
                      value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_102_5 = Coupling(name = 'R2GC_102_5',
                      value = '-0.005208333333333333*(cw*ee*complex(0,1)*G**3)/(cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_102_6 = Coupling(name = 'R2GC_102_6',
                      value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_103_7 = Coupling(name = 'R2GC_103_7',
                      value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_103_8 = Coupling(name = 'R2GC_103_8',
                      value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_104_9 = Coupling(name = 'R2GC_104_9',
                      value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_104_10 = Coupling(name = 'R2GC_104_10',
                       value = '-0.003472222222222222*(ee**2*complex(0,1)*G**2)/cmath.pi**2 + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_105_11 = Coupling(name = 'R2GC_105_11',
                       value = '-0.08333333333333333*(cw*ee*complex(0,1)*G**2)/(cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_106_12 = Coupling(name = 'R2GC_106_12',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_108_13 = Coupling(name = 'R2GC_108_13',
                       value = '-0.0625*(complex(0,1)*G**2*yb**2)/cmath.pi**2 - (complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_109_14 = Coupling(name = 'R2GC_109_14',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_124_15 = Coupling(name = 'R2GC_124_15',
                       value = '-0.005208333333333333*G**4/cmath.pi**2',
                       order = {'QCD':4})

R2GC_124_16 = Coupling(name = 'R2GC_124_16',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_125_17 = Coupling(name = 'R2GC_125_17',
                       value = '-0.005208333333333333*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_125_18 = Coupling(name = 'R2GC_125_18',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_126_19 = Coupling(name = 'R2GC_126_19',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_126_20 = Coupling(name = 'R2GC_126_20',
                       value = '-0.015625*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_127_21 = Coupling(name = 'R2GC_127_21',
                       value = '-0.020833333333333332*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_129_22 = Coupling(name = 'R2GC_129_22',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_129_23 = Coupling(name = 'R2GC_129_23',
                       value = '-0.03125*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_130_24 = Coupling(name = 'R2GC_130_24',
                       value = 'G**3/(48.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_132_25 = Coupling(name = 'R2GC_132_25',
                       value = '(complex(0,1)*G**3)/(48.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_133_26 = Coupling(name = 'R2GC_133_26',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_134_27 = Coupling(name = 'R2GC_134_27',
                       value = '-0.16666666666666666*(complex(0,1)*G**3)/cmath.pi**2',
                       order = {'QCD':3})

R2GC_135_28 = Coupling(name = 'R2GC_135_28',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_139_29 = Coupling(name = 'R2GC_139_29',
                       value = '-0.1111111111111111*(ee*complex(0,1)*G**2)/cmath.pi**2',
                       order = {'QCD':2,'QED':1})

R2GC_142_30 = Coupling(name = 'R2GC_142_30',
                       value = '-0.16666666666666666*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_146_31 = Coupling(name = 'R2GC_146_31',
                       value = '(complex(0,1)*G**2*MB)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_148_32 = Coupling(name = 'R2GC_148_32',
                       value = '-0.05555555555555555*(ee*complex(0,1)*G**2*sw)/(cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_149_33 = Coupling(name = 'R2GC_149_33',
                       value = '(complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_150_34 = Coupling(name = 'R2GC_150_34',
                       value = '-0.3333333333333333*(G**2*yb)/(cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_158_35 = Coupling(name = 'R2GC_158_35',
                       value = '(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_158_36 = Coupling(name = 'R2GC_158_36',
                       value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_159_37 = Coupling(name = 'R2GC_159_37',
                       value = '(11*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_159_38 = Coupling(name = 'R2GC_159_38',
                       value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_161_39 = Coupling(name = 'R2GC_161_39',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_161_40 = Coupling(name = 'R2GC_161_40',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_163_41 = Coupling(name = 'R2GC_163_41',
                       value = '-0.0625*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_163_42 = Coupling(name = 'R2GC_163_42',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_167_43 = Coupling(name = 'R2GC_167_43',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_170_44 = Coupling(name = 'R2GC_170_44',
                       value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_171_45 = Coupling(name = 'R2GC_171_45',
                       value = '(G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_172_46 = Coupling(name = 'R2GC_172_46',
                       value = '-0.3333333333333333*(G**2*yb)/cmath.pi**2',
                       order = {'QCD':2,'QED':1})

R2GC_173_47 = Coupling(name = 'R2GC_173_47',
                       value = '(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_174_48 = Coupling(name = 'R2GC_174_48',
                       value = '-0.3333333333333333*(G**2*yt)/cmath.pi**2',
                       order = {'QCD':2,'QED':1})

R2GC_175_49 = Coupling(name = 'R2GC_175_49',
                       value = '(G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_176_50 = Coupling(name = 'R2GC_176_50',
                       value = '(complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_82_51 = Coupling(name = 'R2GC_82_51',
                      value = '-0.0625*(complex(0,1)*G**2)/cmath.pi**2',
                      order = {'QCD':2})

R2GC_83_52 = Coupling(name = 'R2GC_83_52',
                      value = '(3*G**3)/(16.*cmath.pi**2)',
                      order = {'QCD':3})

R2GC_95_53 = Coupling(name = 'R2GC_95_53',
                      value = '-0.125*(complex(0,1)*G**2*MB**2)/cmath.pi**2',
                      order = {'QCD':2})

R2GC_95_54 = Coupling(name = 'R2GC_95_54',
                      value = '-0.125*(complex(0,1)*G**2*MT**2)/cmath.pi**2',
                      order = {'QCD':2})

R2GC_96_55 = Coupling(name = 'R2GC_96_55',
                      value = '-0.125*(complex(0,1)*G**2*MB*yb)/(cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_96_56 = Coupling(name = 'R2GC_96_56',
                      value = '-0.125*(complex(0,1)*G**2*MT*yt)/(cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_97_57 = Coupling(name = 'R2GC_97_57',
                      value = '-0.0625*(complex(0,1)*G**2*yb**2)/cmath.pi**2',
                      order = {'QCD':2,'QED':2})

R2GC_97_58 = Coupling(name = 'R2GC_97_58',
                      value = '-0.0625*(complex(0,1)*G**2*yt**2)/cmath.pi**2',
                      order = {'QCD':2,'QED':2})

R2GC_98_59 = Coupling(name = 'R2GC_98_59',
                      value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_99_60 = Coupling(name = 'R2GC_99_60',
                      value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_99_61 = Coupling(name = 'R2GC_99_61',
                      value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

UVGC_111_1 = Coupling(name = 'UVGC_111_1',
                      value = {-1:'(53*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_112_2 = Coupling(name = 'UVGC_112_2',
                      value = {-1:'G**3/(32.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_113_3 = Coupling(name = 'UVGC_113_3',
                      value = {-1:'-0.0078125*(complex(0,1)*G**4)/cmath.pi**2'},
                      order = {'QCD':4})

UVGC_114_4 = Coupling(name = 'UVGC_114_4',
                      value = {-1:'-0.08333333333333333*(complex(0,1)*G**2)/cmath.pi**2'},
                      order = {'QCD':2})

UVGC_115_5 = Coupling(name = 'UVGC_115_5',
                      value = {-1:'-0.05555555555555555*(ee*complex(0,1)*G**2)/cmath.pi**2'},
                      order = {'QCD':2,'QED':1})

UVGC_117_6 = Coupling(name = 'UVGC_117_6',
                      value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_122_7 = Coupling(name = 'UVGC_122_7',
                      value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_123_8 = Coupling(name = 'UVGC_123_8',
                      value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_123_9 = Coupling(name = 'UVGC_123_9',
                      value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_124_10 = Coupling(name = 'UVGC_124_10',
                       value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_124_11 = Coupling(name = 'UVGC_124_11',
                       value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_125_12 = Coupling(name = 'UVGC_125_12',
                       value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_125_13 = Coupling(name = 'UVGC_125_13',
                       value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_127_14 = Coupling(name = 'UVGC_127_14',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_128_15 = Coupling(name = 'UVGC_128_15',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_129_16 = Coupling(name = 'UVGC_129_16',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_129_17 = Coupling(name = 'UVGC_129_17',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_130_18 = Coupling(name = 'UVGC_130_18',
                       value = {-1:'G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_131_19 = Coupling(name = 'UVGC_131_19',
                       value = {-1:'-0.041666666666666664*G**3/cmath.pi**2'},
                       order = {'QCD':3})

UVGC_132_20 = Coupling(name = 'UVGC_132_20',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_135_21 = Coupling(name = 'UVGC_135_21',
                       value = {-1:'(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_142_22 = Coupling(name = 'UVGC_142_22',
                       value = {-1:'(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_142_23 = Coupling(name = 'UVGC_142_23',
                       value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_144_24 = Coupling(name = 'UVGC_144_24',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MB else -0.08333333333333333*(complex(0,1)*G**2)/cmath.pi**2 ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_145_25 = Coupling(name = 'UVGC_145_25',
                       value = {-1:'( (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) if MB else -0.027777777777777776*(ee*complex(0,1)*G**2)/cmath.pi**2 )',0:'( (5*ee*complex(0,1)*G**2)/(36.*cmath.pi**2) - (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(6.*cmath.pi**2) if MB else (ee*complex(0,1)*G**2)/(36.*cmath.pi**2) ) - (ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_146_26 = Coupling(name = 'UVGC_146_26',
                       value = {-1:'( (complex(0,1)*G**2*MB)/(6.*cmath.pi**2) if MB else -0.08333333333333333*(complex(0,1)*G**2*MB)/cmath.pi**2 ) + (complex(0,1)*G**2*MB)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MB)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MB*reglog(MB/MU_R))/cmath.pi**2 if MB else (complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MB)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_147_27 = Coupling(name = 'UVGC_147_27',
                       value = {-1:'( (cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MB else -0.041666666666666664*(cw*ee*complex(0,1)*G**2)/(cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(12.*cw*cmath.pi**2) if MB else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_148_28 = Coupling(name = 'UVGC_148_28',
                       value = {-1:'( -0.05555555555555555*(ee*complex(0,1)*G**2*sw)/(cw*cmath.pi**2) if MB else (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',0:'( (-5*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(6.*cw*cmath.pi**2) if MB else -0.027777777777777776*(ee*complex(0,1)*G**2*sw)/(cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_149_29 = Coupling(name = 'UVGC_149_29',
                       value = {-1:'( (complex(0,1)*G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else -0.08333333333333333*(complex(0,1)*G**2*yb)/(cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_150_30 = Coupling(name = 'UVGC_150_30',
                       value = {-1:'( -0.16666666666666666*(G**2*yb)/(cmath.pi**2*cmath.sqrt(2)) if MB else (G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-3*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) + (G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else -0.08333333333333333*(G**2*yb)/(cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_151_31 = Coupling(name = 'UVGC_151_31',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':2})

UVGC_151_32 = Coupling(name = 'UVGC_151_32',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':2})

UVGC_152_33 = Coupling(name = 'UVGC_152_33',
                       value = {-1:'( 0 if MB else -0.0625*G**3/cmath.pi**2 )',0:'( -0.125*(G**3*reglog(MB/MU_R))/cmath.pi**2 if MB else 0 )'},
                       order = {'QCD':3})

UVGC_152_34 = Coupling(name = 'UVGC_152_34',
                       value = {-1:'( 0 if MT else -0.0625*G**3/cmath.pi**2 )',0:'( -0.125*(G**3*reglog(MT/MU_R))/cmath.pi**2 if MT else 0 )'},
                       order = {'QCD':3})

UVGC_153_35 = Coupling(name = 'UVGC_153_35',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**3)/(48.*cmath.pi**2) )',0:'( (complex(0,1)*G**3*reglog(MB/MU_R))/(24.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':3})

UVGC_153_36 = Coupling(name = 'UVGC_153_36',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_153_37 = Coupling(name = 'UVGC_153_37',
                       value = {-1:'-0.0078125*(complex(0,1)*G**3)/cmath.pi**2'},
                       order = {'QCD':3})

UVGC_153_38 = Coupling(name = 'UVGC_153_38',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**3)/(48.*cmath.pi**2) )',0:'( (complex(0,1)*G**3*reglog(MT/MU_R))/(24.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':3})

UVGC_154_39 = Coupling(name = 'UVGC_154_39',
                       value = {-1:'(complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_157_40 = Coupling(name = 'UVGC_157_40',
                       value = {-1:'( -0.16666666666666666*(complex(0,1)*G**3)/cmath.pi**2 if MB else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else -0.08333333333333333*(complex(0,1)*G**3)/cmath.pi**2 ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_158_41 = Coupling(name = 'UVGC_158_41',
                       value = {-1:'( 0 if MB else -0.08333333333333333*(complex(0,1)*G**4)/cmath.pi**2 ) + (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( -0.16666666666666666*(complex(0,1)*G**4*reglog(MB/MU_R))/cmath.pi**2 if MB else 0 )'},
                       order = {'QCD':4})

UVGC_158_42 = Coupling(name = 'UVGC_158_42',
                       value = {-1:'-0.041666666666666664*(complex(0,1)*G**4)/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_158_43 = Coupling(name = 'UVGC_158_43',
                       value = {-1:'(83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_158_44 = Coupling(name = 'UVGC_158_44',
                       value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_158_45 = Coupling(name = 'UVGC_158_45',
                       value = {-1:'( 0 if MT else -0.08333333333333333*(complex(0,1)*G**4)/cmath.pi**2 ) + (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( -0.16666666666666666*(complex(0,1)*G**4*reglog(MT/MU_R))/cmath.pi**2 if MT else 0 )'},
                       order = {'QCD':4})

UVGC_159_46 = Coupling(name = 'UVGC_159_46',
                       value = {-1:'(335*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_159_47 = Coupling(name = 'UVGC_159_47',
                       value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_160_48 = Coupling(name = 'UVGC_160_48',
                       value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_161_49 = Coupling(name = 'UVGC_161_49',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(6.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_161_50 = Coupling(name = 'UVGC_161_50',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_161_51 = Coupling(name = 'UVGC_161_51',
                       value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_161_52 = Coupling(name = 'UVGC_161_52',
                       value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_161_53 = Coupling(name = 'UVGC_161_53',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(6.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_162_54 = Coupling(name = 'UVGC_162_54',
                       value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_163_55 = Coupling(name = 'UVGC_163_55',
                       value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_163_56 = Coupling(name = 'UVGC_163_56',
                       value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_164_57 = Coupling(name = 'UVGC_164_57',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MT else -0.08333333333333333*(complex(0,1)*G**2)/cmath.pi**2 ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_165_58 = Coupling(name = 'UVGC_165_58',
                       value = {-1:'( -0.1111111111111111*(ee*complex(0,1)*G**2)/cmath.pi**2 if MT else (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) )',0:'( (-5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2) if MT else -0.05555555555555555*(ee*complex(0,1)*G**2)/cmath.pi**2 ) + (ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_166_59 = Coupling(name = 'UVGC_166_59',
                       value = {-1:'( -0.16666666666666666*(complex(0,1)*G**3)/cmath.pi**2 if MT else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else -0.08333333333333333*(complex(0,1)*G**3)/cmath.pi**2 ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_167_60 = Coupling(name = 'UVGC_167_60',
                       value = {-1:'( (complex(0,1)*G**2*MT)/(6.*cmath.pi**2) if MT else -0.08333333333333333*(complex(0,1)*G**2*MT)/cmath.pi**2 ) + (complex(0,1)*G**2*MT)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MT)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2 if MT else (complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MT)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_168_61 = Coupling(name = 'UVGC_168_61',
                       value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2)) if MB else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else -0.041666666666666664*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_168_62 = Coupling(name = 'UVGC_168_62',
                       value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -0.041666666666666664*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_169_63 = Coupling(name = 'UVGC_169_63',
                       value = {-1:'( -0.08333333333333333*(cw*ee*complex(0,1)*G**2)/(cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MT else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (-5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2) if MT else -0.041666666666666664*(cw*ee*complex(0,1)*G**2)/(cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_170_64 = Coupling(name = 'UVGC_170_64',
                       value = {-1:'( (ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) if MT else -0.05555555555555555*(ee*complex(0,1)*G**2*sw)/(cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',0:'( (5*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) if MT else (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_171_65 = Coupling(name = 'UVGC_171_65',
                       value = {-1:'( (G**2*yb)/(12.*cmath.pi**2) if MB else -0.041666666666666664*(G**2*yb)/cmath.pi**2 )',0:'( (13*G**2*yb)/(24.*cmath.pi**2) - (3*G**2*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (G**2*yb)/(24.*cmath.pi**2) ) - (G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_171_66 = Coupling(name = 'UVGC_171_66',
                       value = {-1:'( (G**2*yb)/(12.*cmath.pi**2) if MT else -0.041666666666666664*(G**2*yb)/cmath.pi**2 )',0:'( (5*G**2*yb)/(24.*cmath.pi**2) - (G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yb)/(24.*cmath.pi**2) ) - (G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_171_67 = Coupling(name = 'UVGC_171_67',
                       value = {-1:'(G**2*yb)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_172_68 = Coupling(name = 'UVGC_172_68',
                       value = {-1:'( -0.08333333333333333*(G**2*yb)/cmath.pi**2 if MB else (G**2*yb)/(24.*cmath.pi**2) )',0:'( (-13*G**2*yb)/(24.*cmath.pi**2) + (3*G**2*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -0.041666666666666664*(G**2*yb)/cmath.pi**2 ) + (G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_172_69 = Coupling(name = 'UVGC_172_69',
                       value = {-1:'( -0.08333333333333333*(G**2*yb)/cmath.pi**2 if MT else (G**2*yb)/(24.*cmath.pi**2) )',0:'( (-5*G**2*yb)/(24.*cmath.pi**2) + (G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -0.041666666666666664*(G**2*yb)/cmath.pi**2 ) + (G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_172_70 = Coupling(name = 'UVGC_172_70',
                       value = {-1:'-0.3333333333333333*(G**2*yb)/cmath.pi**2'},
                       order = {'QCD':2,'QED':1})

UVGC_173_71 = Coupling(name = 'UVGC_173_71',
                       value = {-1:'( (G**2*yt)/(12.*cmath.pi**2) if MB else -0.041666666666666664*(G**2*yt)/cmath.pi**2 )',0:'( (5*G**2*yt)/(24.*cmath.pi**2) - (G**2*yt*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (G**2*yt)/(24.*cmath.pi**2) ) - (G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_173_72 = Coupling(name = 'UVGC_173_72',
                       value = {-1:'( (G**2*yt)/(12.*cmath.pi**2) if MT else -0.041666666666666664*(G**2*yt)/cmath.pi**2 )',0:'( (13*G**2*yt)/(24.*cmath.pi**2) - (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yt)/(24.*cmath.pi**2) ) - (G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_173_73 = Coupling(name = 'UVGC_173_73',
                       value = {-1:'(G**2*yt)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_174_74 = Coupling(name = 'UVGC_174_74',
                       value = {-1:'( -0.08333333333333333*(G**2*yt)/cmath.pi**2 if MB else (G**2*yt)/(24.*cmath.pi**2) )',0:'( (-5*G**2*yt)/(24.*cmath.pi**2) + (G**2*yt*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -0.041666666666666664*(G**2*yt)/cmath.pi**2 ) + (G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_174_75 = Coupling(name = 'UVGC_174_75',
                       value = {-1:'( -0.08333333333333333*(G**2*yt)/cmath.pi**2 if MT else (G**2*yt)/(24.*cmath.pi**2) )',0:'( (-13*G**2*yt)/(24.*cmath.pi**2) + (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -0.041666666666666664*(G**2*yt)/cmath.pi**2 ) + (G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_174_76 = Coupling(name = 'UVGC_174_76',
                       value = {-1:'-0.3333333333333333*(G**2*yt)/cmath.pi**2'},
                       order = {'QCD':2,'QED':1})

UVGC_175_77 = Coupling(name = 'UVGC_175_77',
                       value = {-1:'( (G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -0.08333333333333333*(G**2*yt)/(cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_176_78 = Coupling(name = 'UVGC_176_78',
                       value = {-1:'( (complex(0,1)*G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -0.08333333333333333*(complex(0,1)*G**2*yt)/(cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

