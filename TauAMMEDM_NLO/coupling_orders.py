# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 13.3.0 for Linux x86 (64-bit) (June 3, 2023)
# Date: Mon 17 Jul 2023 19:23:27


from object_library import all_orders, CouplingOrder


AMMEDM = CouplingOrder(name = 'AMMEDM',
                       expansion_order = 99,
                       hierarchy = 2)

QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1,
                    perturbative_expansion = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

